module Main exposing (main)

import Browser
import Browser.Events
import Element exposing (DeviceClass(..), Element, Orientation(..), centerX, centerY, clipX, column, el, fill, height, inFront, minimum, padding, pointer, px, row, text, width)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Font as Font
import Element.Input as Input
import GoogleMaps.Map as Map
import GoogleMaps.Marker as Marker exposing (Marker)
import Html exposing (Html)
import Html.Attributes
import Http exposing (expectJson, get)
import Json.Decode exposing (int, list, string)
import Json.Decode.Pipeline exposing (required)
import RemoteData exposing (RemoteData(..), WebData)
import Svg exposing (g, svg)
import Svg.Attributes as SvgA



---- MODEL ----


type alias Model =
    { mapType : Map.MapType
    , googleMapKey : String
    , mapReady : Bool
    , searchLocation : String
    , width : Int
    , height : Int
    , showLocationSuggestions : Bool
    , placesSuggestions : WebData PlacesAutocomplete
    , location : WebData GeolocationResult
    , markers : List (Marker Msg)
    , latitude : Float
    , longitude : Float
    }


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( { mapType = Map.roadmap
      , googleMapKey = flags.key
      , mapReady = False
      , width = flags.width
      , height = flags.height
      , searchLocation = ""
      , showLocationSuggestions = False
      , placesSuggestions = NotAsked
      , location = NotAsked
      , markers = []
      , latitude = -32.4845349
      , longitude = -58.2321416
      }
    , Cmd.none
    )


type alias Flags =
    { width : Int
    , height : Int
    , key : String
    }


type alias PlacesAutocomplete =
    { predictions : List Place
    , status : String
    }


type alias Place =
    { description : String
    , id : Maybe String
    , matched_substrings : List SubstringOffset
    , place_id : Maybe String
    , reference : Maybe String
    , structured_formatting : StructuredFormatting
    , terms : List Offset
    , types : Maybe (List String)
    }


type alias StructuredFormatting =
    { main_text : String
    , main_text_matched_substrings : List SubstringOffset
    , secondary_text : Maybe String
    }


type alias SubstringOffset =
    { length : Int
    , offset : Int
    }


type alias Offset =
    { offset : Int
    , value : String
    }


type alias GeolocationResult =
    { results : List Geolocation
    , status : String
    }


type alias Geolocation =
    { address_components : List AddressComponents
    , formatted_address : String
    , geometry : Geometry
    , place_id : String
    , plus_code : Maybe PlusCode
    , types : Maybe (List String)
    }


type alias LatLong =
    { lat : Float
    , lng : Float
    }


type alias Viewport =
    { northeast : LatLong
    , southwest : LatLong
    }


type alias Geometry =
    { location : LatLong
    , location_type : String
    , viewport : Viewport
    }


type alias PlusCode =
    { compound_code : String
    , global_code : String
    }


type alias AddressComponents =
    { long_name : String
    , short_name : String
    , types : List String
    }



---- UPDATE ----


type Msg
    = MapReady
    | WindowResized Int Int
    | ActivateSearchLocationTextInput
    | DeactivateSearchLocationTextInput
    | SearchLocationTextInputChanged String
    | LocationSuggestionClicked String
    | GotPlacesSuggestions (WebData PlacesAutocomplete)
    | GotLocation (WebData GeolocationResult)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        MapReady ->
            ( { model | mapReady = True }, Cmd.none )

        SearchLocationTextInputChanged newLocation ->
            if String.length newLocation >= 3 then
                ( { model | searchLocation = newLocation, showLocationSuggestions = True, placesSuggestions = Loading }, getPlacesSuggestions model )

            else
                ( { model | searchLocation = newLocation, showLocationSuggestions = False }, Cmd.none )

        LocationSuggestionClicked location ->
            ( { model | searchLocation = location, showLocationSuggestions = False }, Cmd.batch [ getLocation model location, getPlacesSuggestions model ] )

        WindowResized width height ->
            ( { model | width = width, height = height }, Cmd.none )

        ActivateSearchLocationTextInput ->
            ( { model | showLocationSuggestions = String.length model.searchLocation >= 3 }, Cmd.none )

        DeactivateSearchLocationTextInput ->
            ( { model | showLocationSuggestions = False }, Cmd.none )

        GotPlacesSuggestions response ->
            ( { model | placesSuggestions = response }, Cmd.none )

        GotLocation response ->
            case response of
                Success result ->
                    let
                        coordinates =
                            getCoordinates result
                    in
                    ( { model | location = response, markers = List.singleton (Marker.init coordinates.lat coordinates.lng), latitude = coordinates.lat, longitude = coordinates.lng }, Cmd.none )

                _ ->
                    ( model, Cmd.none )


getCoordinates : GeolocationResult -> LatLong
getCoordinates geolocationResult =
    let
        firstResult =
            List.head geolocationResult.results
    in
    case firstResult of
        Just result ->
            result.geometry.location

        _ ->
            LatLong 0 0


getPlacesSuggestions : Model -> Cmd Msg
getPlacesSuggestions model =
    Http.get
        { url = "https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api/place/queryautocomplete/json?key=" ++ model.googleMapKey ++ "&input=" ++ model.searchLocation
        , expect = expectJson (RemoteData.fromResult >> GotPlacesSuggestions) decodePlacesSuggestions
        }


decodePlacesSuggestions : Json.Decode.Decoder PlacesAutocomplete
decodePlacesSuggestions =
    Json.Decode.succeed PlacesAutocomplete
        |> Json.Decode.Pipeline.required "predictions" (Json.Decode.list decodePlace)
        |> Json.Decode.Pipeline.required "status" Json.Decode.string


decodePlace : Json.Decode.Decoder Place
decodePlace =
    Json.Decode.succeed Place
        |> Json.Decode.Pipeline.required "description" Json.Decode.string
        |> Json.Decode.Pipeline.optional "id" (Json.Decode.maybe Json.Decode.string) Nothing
        |> Json.Decode.Pipeline.required "matched_substrings" (Json.Decode.list decodeSubstringOffset)
        |> Json.Decode.Pipeline.optional "place_id" (Json.Decode.maybe Json.Decode.string) Nothing
        |> Json.Decode.Pipeline.optional "reference" (Json.Decode.maybe Json.Decode.string) Nothing
        |> Json.Decode.Pipeline.required "structured_formatting" decodeStructuredFormatting
        |> Json.Decode.Pipeline.required "terms" (Json.Decode.list decodeOffset)
        |> Json.Decode.Pipeline.optional "types" (Json.Decode.maybe (Json.Decode.list Json.Decode.string)) Nothing


decodeStructuredFormatting : Json.Decode.Decoder StructuredFormatting
decodeStructuredFormatting =
    Json.Decode.succeed StructuredFormatting
        |> Json.Decode.Pipeline.required "main_text" Json.Decode.string
        |> Json.Decode.Pipeline.required "main_text_matched_substrings" (Json.Decode.list decodeSubstringOffset)
        |> Json.Decode.Pipeline.optional "secondary_text" (Json.Decode.maybe Json.Decode.string) Nothing


decodeSubstringOffset : Json.Decode.Decoder SubstringOffset
decodeSubstringOffset =
    Json.Decode.succeed SubstringOffset
        |> Json.Decode.Pipeline.required "length" Json.Decode.int
        |> Json.Decode.Pipeline.required "offset" Json.Decode.int


decodeOffset : Json.Decode.Decoder Offset
decodeOffset =
    Json.Decode.succeed Offset
        |> Json.Decode.Pipeline.required "offset" Json.Decode.int
        |> Json.Decode.Pipeline.required "value" Json.Decode.string


getLocation : Model -> String -> Cmd Msg
getLocation model location =
    Http.get
        { url = "https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api/geocode/json?address=" ++ location ++ "&key=" ++ model.googleMapKey
        , expect = expectJson (RemoteData.fromResult >> GotLocation) decodeGeolocationResult
        }


decodeGeolocationResult : Json.Decode.Decoder GeolocationResult
decodeGeolocationResult =
    Json.Decode.succeed GeolocationResult
        |> Json.Decode.Pipeline.required "results" (Json.Decode.list decodeGeolocation)
        |> Json.Decode.Pipeline.required "status" Json.Decode.string


decodeLatLong : Json.Decode.Decoder LatLong
decodeLatLong =
    Json.Decode.succeed LatLong
        |> Json.Decode.Pipeline.required "lat" Json.Decode.float
        |> Json.Decode.Pipeline.required "lng" Json.Decode.float


decodeViewport : Json.Decode.Decoder Viewport
decodeViewport =
    Json.Decode.succeed Viewport
        |> Json.Decode.Pipeline.required "northeast" decodeLatLong
        |> Json.Decode.Pipeline.required "southwest" decodeLatLong


decodeGeometry : Json.Decode.Decoder Geometry
decodeGeometry =
    Json.Decode.succeed Geometry
        |> Json.Decode.Pipeline.required "location" decodeLatLong
        |> Json.Decode.Pipeline.required "location_type" Json.Decode.string
        |> Json.Decode.Pipeline.required "viewport" decodeViewport


decodePlusCode : Json.Decode.Decoder PlusCode
decodePlusCode =
    Json.Decode.succeed PlusCode
        |> Json.Decode.Pipeline.required "compound_code" Json.Decode.string
        |> Json.Decode.Pipeline.required "global_code" Json.Decode.string


decodeGeolocation : Json.Decode.Decoder Geolocation
decodeGeolocation =
    Json.Decode.succeed Geolocation
        |> Json.Decode.Pipeline.required "address_components" (Json.Decode.list decodeAddressComponents)
        |> Json.Decode.Pipeline.required "formatted_address" Json.Decode.string
        |> Json.Decode.Pipeline.required "geometry" decodeGeometry
        |> Json.Decode.Pipeline.required "place_id" Json.Decode.string
        |> Json.Decode.Pipeline.optional "plus_code" (Json.Decode.maybe decodePlusCode) Nothing
        |> Json.Decode.Pipeline.optional "types" (Json.Decode.maybe (Json.Decode.list Json.Decode.string)) Nothing


decodeAddressComponents : Json.Decode.Decoder AddressComponents
decodeAddressComponents =
    Json.Decode.succeed AddressComponents
        |> Json.Decode.Pipeline.required "long_name" Json.Decode.string
        |> Json.Decode.Pipeline.required "short_name" Json.Decode.string
        |> Json.Decode.Pipeline.required "types" (Json.Decode.list Json.Decode.string)


webDataView : (a -> Element Msg) -> WebData a -> Element Msg
webDataView successView webData =
    case webData of
        NotAsked ->
            Element.none

        Loading ->
            el
                [ centerX
                , centerY
                , padding 10
                , width (px 400)
                , Background.color (Element.rgb255 255 255 255)
                , Border.color (Element.rgb255 186 189 182)
                , Border.width 1
                , Border.roundEach { topLeft = 0, topRight = 0, bottomLeft = 8, bottomRight = 8 }
                , Border.shadow
                    { offset = ( 0, 3 ), blur = 6, size = 0, color = Element.rgba 0 0 0 0.32 }
                ]
            <|
                el [ centerX, centerY, Element.htmlAttribute <| Html.Attributes.style "-webkit-animation" "1s spinner infinite" ] <|
                    loadingSvg

        Failure err ->
            case err of
                Http.BadUrl url ->
                    el
                        [ centerX
                        , centerY
                        , padding 10
                        , width (px 400)
                        , Background.color (Element.rgb255 255 255 255)
                        , Border.color (Element.rgb255 186 189 182)
                        , Border.width 1
                        , Border.roundEach { topLeft = 0, topRight = 0, bottomLeft = 8, bottomRight = 8 }
                        , Border.shadow
                            { offset = ( 0, 3 ), blur = 6, size = 0, color = Element.rgba 0 0 0 0.32 }
                        ]
                    <|
                        text <|
                            "Bad URL: "
                                ++ url

                Http.Timeout ->
                    el
                        [ centerX
                        , centerY
                        , padding 10
                        , width (px 400)
                        , Background.color (Element.rgb255 255 255 255)
                        , Border.color (Element.rgb255 186 189 182)
                        , Border.width 1
                        , Border.roundEach { topLeft = 0, topRight = 0, bottomLeft = 8, bottomRight = 8 }
                        , Border.shadow
                            { offset = ( 0, 3 ), blur = 6, size = 0, color = Element.rgba 0 0 0 0.32 }
                        ]
                    <|
                        text <|
                            "Connection Timeout"

                Http.NetworkError ->
                    el
                        [ centerX
                        , centerY
                        , padding 10
                        , width (px 400)
                        , Background.color (Element.rgb255 255 255 255)
                        , Border.color (Element.rgb255 186 189 182)
                        , Border.width 1
                        , Border.roundEach { topLeft = 0, topRight = 0, bottomLeft = 8, bottomRight = 8 }
                        , Border.shadow
                            { offset = ( 0, 3 ), blur = 6, size = 0, color = Element.rgba 0 0 0 0.32 }
                        ]
                    <|
                        text <|
                            "Couldn't Connect"

                Http.BadStatus statusCode ->
                    el
                        [ centerX
                        , centerY
                        , padding 10
                        , width (px 400)
                        , Background.color (Element.rgb255 255 255 255)
                        , Border.color (Element.rgb255 186 189 182)
                        , Border.width 1
                        , Border.roundEach { topLeft = 0, topRight = 0, bottomLeft = 8, bottomRight = 8 }
                        , Border.shadow
                            { offset = ( 0, 3 ), blur = 6, size = 0, color = Element.rgba 0 0 0 0.32 }
                        ]
                    <|
                        text <|
                            "Couldn't Retrieve Data: "
                                ++ String.fromInt statusCode

                Http.BadBody error ->
                    el
                        [ centerX
                        , centerY
                        , padding 10
                        , width (px 400)
                        , Background.color (Element.rgb255 255 255 255)
                        , Border.color (Element.rgb255 186 189 182)
                        , Border.width 1
                        , Border.roundEach { topLeft = 0, topRight = 0, bottomLeft = 8, bottomRight = 8 }
                        , Border.shadow
                            { offset = ( 0, 3 ), blur = 6, size = 0, color = Element.rgba 0 0 0 0.32 }
                        ]
                    <|
                        text <|
                            "Bad response, please contact the developer: "
                                ++ error

        Success data ->
            successView data


loadingSvg : Element msg
loadingSvg =
    Element.html <|
        svg
            [ SvgA.width "20"
            , SvgA.height "20"
            , SvgA.viewBox "0 0 122 122"
            , SvgA.version "1.1"
            ]
            [ g
                [ SvgA.id "Page-1"
                , SvgA.stroke "none"
                , SvgA.strokeWidth "1"
                , SvgA.fill "none"
                , SvgA.fillRule "evenodd"
                ]
                [ g
                    [ SvgA.id "styles"
                    , SvgA.transform "translate(-860.000000, -5380.000000)"
                    , SvgA.fill "#101010"
                    ]
                    [ g
                        [ SvgA.id "spinner-light"
                        , SvgA.transform "translate(860.000000, 5380.000000)"
                        ]
                        [ Svg.path
                            [ SvgA.d "M18.5735931,18.5735931 C-4.85786437,42.0050506 -4.85786437,79.9949494 18.5735931,103.426407 C42.0050506,126.857864 79.9949494,126.857864 103.426407,103.426407 L97.7695526,97.7695526 C77.4622895,118.076816 44.5377105,118.076816 24.2304474,97.7695526 C3.92318421,77.4622895 3.92318421,44.5377105 24.2304474,24.2304474 L18.5735931,18.5735931 L18.5735931,18.5735931 L18.5735931,18.5735931 Z"
                            , SvgA.id "spinner"
                            ]
                            []
                        ]
                    ]
                ]
            ]



---- SUBS ----


subscriptions : Model -> Sub Msg
subscriptions _ =
    Browser.Events.onResize WindowResized



---- VIEW ----


googleMapView : Model -> Html Msg
googleMapView model =
    Map.init model.googleMapKey
        |> Map.withMapType model.mapType
        |> Map.withFitToMarkers True
        |> Map.onMapReady MapReady
        |> Map.withZoom 13
        |> Map.withMarkers model.markers
        |> Map.withCenter model.latitude model.longitude
        |> Map.withPolygons []
        |> Map.toHtml


view : Model -> Html Msg
view model =
    Element.layoutWith
        { options =
            [ Element.focusStyle
                { borderColor = Maybe.Nothing
                , backgroundColor = Maybe.Nothing
                , shadow = Maybe.Nothing
                }
            ]
        }
        [ width (fill |> minimum model.width)
        , height (px model.height)
        , Font.family
            [ Font.external
                { name = "Roboto"
                , url = "https://fonts.googleapis.com/css?family=Roboto"
                }
            , Font.sansSerif
            ]
        ]
    <|
        column [ inFront <| searchLocationTextInput model ]
            [ el [ width (fill |> minimum model.width), height (fill |> minimum model.height) ] <| Element.html <| googleMapView model
            ]


searchLocationTextInput : Model -> Element Msg
searchLocationTextInput model =
    if model.mapReady then
        el
            [ padding 10
            , Font.size 15
            , centerX
            ]
        <|
            Input.search
                [ centerY
                , Events.onFocus ActivateSearchLocationTextInput
                , Events.onLoseFocus DeactivateSearchLocationTextInput
                , placesAutocompleteDropdown model
                , width (px 400)
                , Input.focusedOnLoad
                , if model.showLocationSuggestions then
                    Border.roundEach { topLeft = 8, topRight = 8, bottomLeft = 0, bottomRight = 0 }

                  else
                    Border.rounded 8
                , Border.shadow
                    { offset = ( 0, 3 ), blur = 6, size = 0, color = Element.rgba 0 0 0 0.32 }
                ]
                { onChange = SearchLocationTextInputChanged
                , text = model.searchLocation
                , placeholder = Maybe.Just (Input.placeholder [ centerY ] (el [] <| text "Search Location"))
                , label = Input.labelHidden "Search Location"
                }

    else
        Element.none


placesAutocompleteDropdown : Model -> Element.Attribute Msg
placesAutocompleteDropdown model =
    Element.below <|
        if model.showLocationSuggestions then
            webDataView (placesSuggestions LocationSuggestionClicked) model.placesSuggestions

        else
            Element.none


placesSuggestions : (String -> Msg) -> PlacesAutocomplete -> Element Msg
placesSuggestions suggestionClickedMsg placesAutocomplete =
    let
        suggestionLists =
            List.map (\place -> place.description) placesAutocomplete.predictions
    in
    column
        [ Font.size 13
        , Background.color (Element.rgb255 255 255 255)
        , Border.color (Element.rgb255 186 189 182)
        , Border.width 1
        , Border.roundEach { topLeft = 0, topRight = 0, bottomLeft = 8, bottomRight = 8 }
        , Border.shadow
            { offset = ( 0, 3 ), blur = 6, size = 0, color = Element.rgba 0 0 0 0.32 }
        , centerX
        , width (px 400)
        , clipX
        ]
        (List.map
            (\suggestion ->
                Input.button
                    [ Events.onMouseDown (suggestionClickedMsg suggestion)
                    , pointer
                    , width fill
                    ]
                    { onPress = Nothing
                    , label =
                        row
                            [ padding 10
                            , Element.mouseOver
                                [ Background.color (Element.rgba 0 0 0 0.06)
                                ]
                            , width fill
                            ]
                            [ text <| suggestion
                            ]
                    }
            )
            suggestionLists
        )



---- PROGRAM ----


main : Program Flags Model Msg
main =
    Browser.element
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        }
