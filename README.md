# Paack Interview Quiz - Elm App


This project is bootstrapped with [Create Elm App](https://github.com/halfzebra/create-elm-app).

It makes use of [PaackEng/elm-google-maps](https://package.elm-lang.org/packages/PaackEng/elm-google-maps/latest/) for showing the map and marker, [mdgriffith/elm-ui](https://package.elm-lang.org/packages/mdgriffith/elm-ui/latest/) for style and [krisajenkins/remotedata](https://package.elm-lang.org/packages/krisajenkins/remotedata/latest/) for fetching data from remote sources.

For the autocomplete and geolocation features I simply fetch the information directly from Google's API: 
* [Places](https://developers.google.com/places/web-service/intro)
* [Geocoding](https://developers.google.com/maps/documentation/geocoding/start)


## Demo
![Demo Video](paackInterviewQuiz.mp4)

## NOTICE

For this to work properly I'm leveragin https://cors-anywhere.herokuapp.com/ since probably I should use the Google Maps JavaScript Library and Elm Ports in order to avoid CORS related issues.

